using namespace System.Security
param ($VmssType)

Set-ExecutionPolicy Unrestricted -Scope LocalMachine -Confirm:$False -Force

New-EventLog -LogName Application -Source 'installCert4Containers.ps1'

#Config
$thumbprint = "06B8EB07624D879B830226696CE20F285C511B13"
$tenantId = "2422c2d3-cb7d-47ae-aff9-725e11b9b2ac"
$applicationId = "59bb8449-aa3d-49ad-bbf9-558a01919784"
$keyVaultName = "keyvault-containers"
$dmzCerts = "cert-terminals-alianza","cert-terminalserver-29oct-qa","cert-terminalserver-ccca-qa","cert-terminalserver-cooprogreso-qa",`
            "cert-iso-cooprogreso-qa","cert-iso-29oct-qa","cert-iso-alianza",`
            "phihub-containers"

$swCerts = "phihub-containers"

$cdeCerts = "phihub-containers","keyvault-qa-cer","keyvault-test", "phicrypto-qa-cer",`
            "cert-hsm-alianza","cert-hsmservice-29oct-qa","cert-hsmservice-ccca-qa","cert-hsmservice-cooprogreso-qa",`
            "cert-auth-29oct-qa","cert-auth-ccca-qa","cert-auth-cooprogreso-qa",`
            "cert-iso-cooprogreso-qa","cert-iso-29oct-qa","cert-iso-alianza",`
            "cert-terminals-alianza","cert-terminalserver-29oct-qa","cert-terminalserver-ccca-qa","cert-terminalserver-cooprogreso-qa"
#/Config

if ($VmssType -eq "DMZ") {
    $certsToInstall = $dmzCerts
}
elseif ($VmssType -eq "SW") {
    $certsToInstall = $swCerts
}
elseif ($VmssType -eq "CDE") {
    $certsToInstall = $cdeCerts
}elseif ($VmssType -eq "ALL"){
	$certsToInstall=$dmzCerts+$swCerts+$cdeCerts| select -Unique
}

#No config
$tempPath = $env:Temp
$loginFlag = $TRUE
$reintento = $TRUE
$count = 0
$countLogin = 0
$Flags = [Cryptography.X509Certificates.X509KeyStorageFlags]::MachineKeySet `
    -bor [Cryptography.X509Certificates.X509KeyStorageFlags]::PersistKeySet `
    -bor [Cryptography.X509Certificates.X509KeyStorageFlags]::Exportable

while ($reintento) {
    try {
        if ($count -eq 10) {
            $reintento = $FALSE
        }
        $count++
		
        Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 100 -Message "Init installing certs"

        if (-Not(Get-Module -ListAvailable -Name AzureRm)) {
            Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 100 -Message "Installing dependencies.... "
            Install-Module AzureRm -Scope AllUsers -Repository PSGallery -AllowClobber -Force
        }
        
        while ($loginFlag) {
            $certLoginAzure = Get-ChildItem -Path Cert:\LocalMachine\My | Where-Object { $_.Thumbprint -eq $thumbprint }
            
            if ($certLoginAzure) {
                Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 100 -Message "Certificado existe... $certLoginAzure.SerialNumber"
                Import-Module AzureRm 
                Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 200 -Message "Modulos cargados..."
                $login = Login-AzureRmAccount -ServicePrincipal -CertificateThumbprint $thumbprint -ApplicationId $applicationId -Tenant $tenantId 
                Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 300 -Message "Login exitoso"
                $loginFlag = $FALSE
            }else {
                if ($countLogin -eq 10) {
                    $loginFlag = $FALSE
                }
                Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 999 -Message "No se ha podido encontrar los certificado de login"
                Start-Sleep -s 15
                $countLogin++
            }
        }

        Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 400 -Message "Init installing certs... "

        $certsToInstall | ForEach-Object {
			Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 500 -Message "Cert name: $_"
            $certVersions = Get-AzureKeyVaultSecret -VaultName $KeyVaultName -Name $_ -IncludeVersions | Where-Object {$_.Enabled}

            $certVersions | ForEach-Object{
                $output=$_.Version
                $unitCert = Get-AzureKeyVaultSecret -VaultName $KeyVaultName -Name $_.Name -Version $output
                Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 500 -Message "Cert Version: $output"
                
                if ($login -and $unitCert) { 
                    $certBytes = [System.Convert]::FromBase64String($unitCert.SecretValueText)
                    $certCollection = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2Collection
                    $certCollection.Import($certBytes, $null, $Flags)
                    $protectedCertificateBytes = $certCollection.Export([System.Security.Cryptography.X509Certificates.X509ContentType]::Pkcs12, $password)
                    $nameCert=$unitCert.Name+"-"+$unitCert.Version
                    $pfxPath = Join-Path -Path $tempPath -ChildPath $nameCert
                    $pfxPath += ".pfx"
                    [System.IO.File]::WriteAllBytes($pfxPath, $protectedCertificateBytes)
                    $secure = ConvertTo-SecureString -String $unitCert.SecretValueText -AsPlainText -Force
                    $certInstalled = Import-PfxCertificate -FilePath $pfxPath Cert:\LocalMachine\My -Password $secure -Exportable
                    Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 600 -Message "Cert installing successful... $certInstalled.Thumbprint"
                }
                else {
                    Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 999 -Message "No se ha podido acceder al certificado"
                }
            }
        }

        Write-Host "Finish install certs"
        $reintento = $FALSE

        #Change to the location of the personal certificates
        #Set-Location Cert:\CurrentUser\My
        
        #Change to the location of the local machine certificates
        #Set-Location Cert:\LocalMachine\My
        
        #Get the installed certificates in that location
        #Get-ChildItem | Format-Table Subject, FriendlyName, Thumbprint -AutoSize
    }
    Catch {
        $ErrorMessage = $_.Exception.Message
        Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 999 -Message $ErrorMessage
        Write-EventLog -LogName Application -Source "installCert4Containers.ps1" -EventID 999 -Message $_.ScriptStackTrace
        Start-Sleep -s 30
    }
}