#Login Azure
Connect-AzAccount

#Obtener Id de la subscripcion
#Get-AzSubscription

#Information about the scale set
$resourceGroup="phihub-containers-ws2019-rg"
$scaleSetName="Node-SF"
$scriptURL="https://sfdgphihubcontainers3741.blob.core.windows.net/customscript-scalesets/installCerts4Containers.ps1"
$subscriptionId= "08dc6347-0164-4fb6-9045-bb18f0598e6d"
$extensionName="installCerts4Containers"

Set-AzContext -SubscriptionId $subscriptionId

$vmss = Get-AzVmss -ResourceGroupName $resourceGroup -VMScaleSetName $scaleSetName

#Set config
$customConfig = @{
"fileUris" = (,$scriptURL);
"commandToExecute" = "powershell -ExecutionPolicy Unrestricted -File installCerts4Containers.ps1"
}

#Add the Custom Script 
$vmss = Add-AzVmssExtension -VirtualMachineScaleSet $vmss -Name $extensionName -Publisher "Microsoft.Compute" -Type "CustomScriptExtension" -TypeHandlerVersion 1.9 -Setting $customConfig

#Update the scale set and apply the Custom Script Extension to the VM instances
Update-AzVmss -ResourceGroupName $resourceGroup -Name $scaleSetName -VirtualMachineScaleSet $vmss